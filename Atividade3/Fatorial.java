import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

//classe para a atividade 3 do cálculo do fatorial;
public class Fatorial extends UnicastRemoteObject implements FatorialInterface {
    private static final long serialVersionUID = 7896795898928782846L;
    private Integer recebido;

    public Fatorial (Integer value) throws RemoteException {
        recebido = value;
    }

    public double calc_fat() throws RemoteException{
        if (recebido > 0){
            return fatorialDoNumero(recebido);
        }

        return 1.0;     
    }

    private static double fatorialDoNumero(int num) {
		int anterior = num-1;
		double fatorialDesteNumero = 0.0;
		if (num <= 1) return 1;
			else fatorialDesteNumero =  num * fatorialDoNumero(anterior);
		return fatorialDesteNumero;
	}
}
  
