import java.rmi.Naming;

class FatorialClient{
    public static void main (String[] argv){
        try{
            FatorialInterface fatorial = (FatorialInterface) Naming.lookup ("//localhost/Fatorial");
            System.out.println("O fatorial é: " + fatorial.calc_fat());    
        } catch (Exception e){
            System.out.println("FatorialClient failed: ");
            e.printStackTrace();        
        }
    }
}
