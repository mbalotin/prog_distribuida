import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FatorialInterface extends Remote {
	// Metodo invocavel remotamente que retorna a mensagem do objeto remoto
	public double calc_fat() throws RemoteException;
}

