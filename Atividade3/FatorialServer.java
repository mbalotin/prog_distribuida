import java.rmi.Naming;
import java.rmi.RemoteException;

class FatorialServer {
	public static void main (String[] argv) {
		try {
			java.rmi.registry.LocateRegistry.createRegistry(1099);
			System.out.println("RMI registry ready.");			
		} catch (RemoteException e) {
			System.out.println("RMI registry already running.");			
		}
		try {
			Naming.rebind ("Fatorial", new Fatorial (Integer.parseInt(argv[0])));
			System.out.println ("FatorialServer is ready.");
		} catch (Exception e) {
			System.out.println ("FatorialServer failed:");
			e.printStackTrace();
		}
	}
}
