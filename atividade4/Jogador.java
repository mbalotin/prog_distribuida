import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.util.Date;

public class Jogador implements JogadorInterface {

	private LocalDateTime lastAction;
	private int jogadorId;

	public int getJogadorId() {
		return jogadorId;
	}

	public void setJogadorId(int jogadorId) {
		this.jogadorId = jogadorId;
	}

	public LocalDateTime getLastAction() {
		return lastAction;
	}

	public void setLastAction(LocalDateTime lastAction) {
		this.lastAction = lastAction;
	}

	@Override
	public void encerrado() throws RemoteException {
		System.out.printf("Jogador %d encerrado pelo Servidor.\n\n", this.getJogadorId());
	}

}
