import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface JogoInterface extends Remote, Serializable {
	public int registra(JogadorInterface jogador) throws RemoteException;

	public int joga(int id) throws RemoteException;

	public int encerra(int id) throws RemoteException;
}