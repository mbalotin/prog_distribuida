import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class Jogo extends UnicastRemoteObject implements JogoInterface {

	private Map<Integer, JogadorInterface> jogadores;
	private int idCount = 1;

	private Timer registryCheck;
	private static final int CHECK_RATE = 1000;

	protected Jogo() throws RemoteException {
		super();
		jogadores = new HashMap<>();
		registryCheck = new Timer("Registry Check");
		registryCheck.scheduleAtFixedRate(new RegistryCheckTask(), 0, CHECK_RATE);
	}

	@Override
	public int registra(JogadorInterface jogador) throws RemoteException {
		((Jogador) jogador).setJogadorId(++idCount);
		jogadores.put(idCount, jogador);
		return idCount;
	}

	@Override
	public int joga(int id) throws RemoteException {
		Jogador jogador = (Jogador) jogadores.get(id);
		if (jogador == null)
			throw new RemoteException("Player not found.");
		System.out.printf("Jogador %s jogou:", jogador.getJogadorId());
		jogador.setLastAction(LocalDateTime.now());
		return 0;
	}

	@Override
	public int encerra(int id) throws RemoteException {
		JogadorInterface jogador = jogadores.remove(id);

		// TODO avisar o Jogador pelo metodo jogador.encerrado;
		jogador.encerrado();
		
		return 0;
	}

	private void registryCheck() throws RemoteException {
		LocalDateTime timeout = LocalDateTime.now().minusHours(1);
		for (JogadorInterface jogador : jogadores.values())
			if (((Jogador) jogador).getLastAction().isBefore(timeout))
				this.encerra(((Jogador) jogador).getJogadorId());
	}

	public class RegistryCheckTask extends TimerTask {
		@Override
		public void run() {
			try {
				Jogo.this.registryCheck();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

}
