import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class ClientMain {

	public static void main(String[] argv) {
		try {
			String host = "localhost";
			if (argv.length > 0)
				host = argv[0];
			JogoInterface jogo = (JogoInterface) Naming.lookup("//" + host + "/Jogo");

			Jogador jogador = new Jogador();
			int id = jogo.registra(jogador);
			jogo.joga(id);
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.out.println("Cliente Falhou");
			e.printStackTrace();
		}
	}

}
