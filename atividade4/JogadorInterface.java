import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface JogadorInterface extends Remote, Serializable {
        public void encerrado() throws RemoteException;

}
